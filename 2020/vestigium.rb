=begin
Vestigium means "trace" in Latin. In this problem we work with Latin squares and matrix traces.
The trace of a square matrix is the sum of the values on the main diagonal (which runs from the upper left to the lower right).
An N-by-N square matrix is a Latin square if each cell contains one of N different values, and no value is repeated within a row 
or a column. In this problem, we will deal only with "natural Latin squares" in which the N values are the integers between 1 
and N.
Given a matrix that contains only integers between 1 and N, we want to compute its trace and check whether it is a natural Latin 
square. To give some additional information, instead of simply telling us whether the matrix is a natural Latin square or not, 
please compute the number of rows and the number of columns that contain repeated values.
=end

test_cases = gets.chomp.to_i

test_cases.times do |test_case|
  @rows       = []
  trace       = 0
  @dup_rows   = 0
  @dup_cols   = 0
  current_row = nil
  size        = gets.chomp.to_i

  size.times do |i|
    current_row = gets.chomp.split(' ')
    @dup_rows  += 1 if current_row.uniq.count != current_row.count

    trace += current_row[i].to_i
    @rows << current_row
  end

  size.times do |row_index|
    list = size.times.map { |i| @rows[i][row_index] }
    @dup_cols += 1 if list.uniq.count != list.count
  end

  puts "Case ##{test_case + 1}: #{trace} #{@dup_rows} #{@dup_cols}"
end
