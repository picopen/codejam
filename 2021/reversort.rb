=begin
Reversort is an algorithm to sort a list of distinct integers in increasing order. The algorithm is based on the "Reverse" 
operation. Each application of this operation reverses the order of some contiguous part of the list.
The most expensive part of executing the algorithm on our architecture is the Reverse operation. Therefore, our measure for the 
cost of each iteration is simply the length of the sublist passed to Reverse, that is, the value j−i+1
The cost of the whole algorithm is the sum of the costs of each iteration.
Given the initial list, compute the cost of executing Reversort on it.
=end

def cost(n, list)
  list = list.split.map(&:to_i)
  total = 0

  n.times do |i|
    idx = list.index(list.min)
    total = total + idx + 1
    list = list[0...idx].reverse + list[idx+1..-1]
  end

  total - 1
end

test_cases = gets.chomp.to_i

(1..test_cases).each do |i|
  n = gets.chomp.to_i
  list = gets.chomp
  puts "Case ##{i}: #{cost(n, list)}"
end
